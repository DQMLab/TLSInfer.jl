using Random, BenchmarkTools, Test
include(joinpath(dirname(@__DIR__),"src","findpeaks.jl"))

data = 0.0 : 0.05 : 1

pk_data = rand(data,200)
prominence = rand(data)
findpeaks(pk_data,prominence,ends=true)

@time for i = 1:10_000
    local pk_data = rand(data,200)
    local prominence = rand(data)
    findpeaks(pk_data,prominence,ends=true)
end

@benchmark findpeaks(pk_data,prominence,ends=true) setup = (pk_data=rand(data,200); prominence=rand(data))


# should detect 2 peaks with ends=false
@test findpeaks(Float32[1.1,1.1,1.2,0.2,1.1,0.5,1.3,1.8,1.3,0.5,0.5,0.6,1.2,1.1],0.2,ends=false) ==
    (Float32[5,8],Float32[1.1,1.8])

# should detect 4 peaks with ends=true
@test findpeaks(Float32[1.1,1.1,1.2,0.2,1.1,0.5,1.3,1.8,1.3,0.5,0.5,0.6,1.2,1.1],0.2,ends=true) ==
    (Float32[3,5,8,13],Float32[1.2,1.1,1.8,1.2])

# shouldn't detect any peak here even with ends=true
@test findpeaks(Float32[1.1,1.2,1.3],0.5,ends=true) ==
    (Float32[],Float32[])

# should detect 1 peak with ends=true
@test findpeaks(Float32[0.79,1.2,1.3],0.5,ends=true) ==
    (Float32[3],Float32[1.3])
