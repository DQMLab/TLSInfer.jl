# Test that TLSs with various coupling strengths end up binned corrected by the Octave detection
# Testing method:
# 1. generate a few TLSs (f,g) pairs
# 2. sample the spectrum according to the Octave algorithm
# 3. run the detection algorithm
# 4. confirm that the detections are accurate
using TLSInfer, TLSInfer.OctaveAnalysis

function sample_spectrum(bin_info, TLSs, nsamples=5)
    bin_Pe = Vector{Float64}(undef,size(bin_info,1))
    for i in eachindex(bin_Pe)
        # grab bin info
        (fmin, fmax, tmin, tmax) = bin_info[i,:]
        # get measurement params
        f = fmin .+ rand(nsamples).*(fmax-fmin)
        t = tmax ./ (rand(nsamples) .+ 1)
        # find nearest TLS
        fTLS,gTLS = TLSs[last(findmin([abs(fTLS-mean(f)) for (fTLS,_) in TLSs]))]
        # compute Pe
        Δω = @. (fTLS-f)*2π
        g = gTLS*2π
        bin_Pe[i] = mean(TLSInfer.model_Pe.(t,Δω,g))
    end
    return bin_Pe
end

function gen_bin_info(f_min, f_max, octave_final)
    # calculated quantities
    Δ = f_max - f_min
    g0 = Δ/2
    n_bins = 2^(octave_final+1) - 1
    bin_info = Matrix{Float64}(undef,n_bins,4)
    i = 0
    for o in 0:octave_final
        g_octave = g0/2^o
        for b in 0:2^o-1
            fmin = f_min + 2*g_octave*b
            fmax = fmin + 2*g_octave
            tmin = 1/(4*g_octave)
            tmax = 1/(2*g_octave)
            bin_info[i+=1,:] .= (fmin, fmax, tmin, tmax)
        end
    end
    # rows correspond to a bin (fmin, fmax, tmin, tmax)
    return bin_info
end

TLSs = [
    (4400,6.35),
    (4553,2.77),
    (4599,3.98),
    (4611,1.64),
    (4731,1.57),
]
f_min = 4146.0
f_max = 5170.0
octave_final = 8

bin_info = gen_bin_info(f_min, f_max, octave_final)
bin_P = 1 .- sample_spectrum(bin_info, TLSs)

# detect
OctaveAnalysis.detect_tls(bin_info.*[1e6 1e6 1e-6 1e-6], bin_P, 0.4)
