using DelimitedFiles, Random
include(joinpath(@__DIR__,"..","src","octaveanalysis.jl"))

bin_info = readdlm(joinpath(@__DIR__,"..","data","bininfo.csv"),',',Float32)
bin_Pes = readdlm(joinpath(@__DIR__,"..","data","binPe.csv"),',',Float32)

#I = [4, 5, 8, 12, 29] # used in article v1
I = randperm(size(bin_Pes,2))[1:5] # use only 5 samples per bin
I = partialsortperm(vec(bin_Pes[310,:]),1:5)

prominence = 0.09

bin_Pe = vec(mean(bin_Pes[:,I],dims=2))
bin_P = 1 .- bin_Pe
particles = OctaveAnalysis.gen_particles(100, bin_info, bin_P, prominence);
