using Schrodinger, PyPlot, BenchmarkTools

function model_tls(tf,Δω,g)
    H  = -Δω/2*(σz⊗σ0) + g*(σ₊⊗σ₋ + σ₋⊗σ₊)
    ψ₀ = qb"10"
    ts = LinRange(0,tf,601)
    s  = sesolve(H,ψ₀,extrema(ts),saveat=ts,abstol=1E-16,reltol=1E-16)
    return abs2.(getindex.(s.states,3))
end


function model_drive(tf,Δω,g)
    H  = -Δω/2*σz + g*σx
    ψ₀ = qb"1"
    ts = LinRange(0,tf,601)
    s  = sesolve(H,ψ₀,extrema(ts),saveat=ts,abstol=1E-16,reltol=1E-16)
    return abs2.(getindex.(s.states,2))
end


g = 0.005 * 2π
tf = 1500
Δω = LinRange(-0.005*2pi,0.005*2pi,201)

A = mapreduce(Δω->model_tls(tf,Δω,g),hcat,Δω)
B = mapreduce(Δω->model_drive(tf,Δω,g),hcat,Δω)

maximum(A.-B)
