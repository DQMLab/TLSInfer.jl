using Schrodinger, PyPlot, BenchmarkTools

function model_Pe_decoherence(t,Δω,g,T₁=13_000,Tϕ=300)
    H  = -Δω/2*(σz⊗σ0) + g*(σ₊⊗σ₋ + σ₋⊗σ₊)
    C  = (√(1/T₁)*(σ₋⊗σ0), √(1/2Tϕ)*(σz⊗σ0))
    ρ₀ = Operator(dense(qb"10"))
    ts = (0.0,t)
    s  = mesolve(H,C,ρ₀,ts,save_start=false,save_end=true,save_everystep=false)
    return real(s.states[][3,3])
end

function create_model_Pe_decoherence_fun(T₁=13_000,Tϕ=300)
    Δω = 0.010*2π
    g = 0.007*2π
    H  = -Δω/2*(σz⊗σ0) + g*(σ₊⊗σ₋ + σ₋⊗σ₊)
    C  = (√(1/T₁)*(σ₋⊗σ0), √(1/2Tϕ)*(σz⊗σ0))
    ρ₀ = Operator(dense(qb"10"))
    L = LindbladEvo(H,C)
    solver = Schrodinger.Tsit5()
    function fun(t,Δω,g)
        ts = (0.0,t)
        update_L_params!(L,Δω,g)
        s = lsolve(L,ρ₀,ts,(),solver,save_start=false,save_end=true,save_everystep=false)
        return real(s.states[][3,3])
    end
    return fun
end

function update_L_params!(L,Δω,g)
    @inbounds for i = [1,2,5,6,7,8,10,12,13,15,17,19,20,23,27,29]
        z = L.L₀.nzval[i]
        L.L₀.nzval[i] = complex(real(z),sign(imag(z))*g)
    end
    @inbounds for i = [3,4,9,11,14,16,25,26]
        z = L.L₀.nzval[i]
        L.L₀.nzval[i] = complex(real(z),sign(imag(z))*Δω)
    end
end

function calc_probs(T1=14E3,Tϕ=500)
    𝕀 = qeye(2)
    # Frequencies in GHz
    ωqs = range(4.970*2π,5.030*2π,length=201)
    ωd = 5*2π
    g = 0.007*2π
    # Relaxation and dephasing
    γ₁ = 1/T1
    γ₂ = 1/Tϕ
    C = (√(γ₁)*(σ₋⊗𝕀), √(γ₂/2)*(σz⊗𝕀))
    # Starting state is the qb excited
    ρ₀ = Operator(qb"10")
    # Measure Pₑ
    O = (1-σz⊗𝕀)/2
    ts = range(0,1000,length=501)
    tspan = extrema(ts)
    Pₑ = Matrix{Float64}(undef,length(ts),length(ωqs))
    for (i,ωq) in enumerate(ωqs)
        H = -(ωq-ωd)/2*(σz⊗𝕀) + g*(σ₊⊗σ₋ + σ₋⊗σ₊)
        res = mesolve(H,C,ρ₀,tspan,O,saveat=ts)
        Pₑ[:,i] = real.(res.evals)
    end
    return ωqs./2π, ts, Pₑ
end
