using TLSInfer, Schrodinger, PyPlot

t = range(0,10,length=1001) #(0.0, 5.0) # μs
Δω = 0.1*2π # MHz
g = 0.05*2π # MHz
Γ = 1.0 # MHz
T₁ = 10.0 # μs


Pe1 = TLSInfer.model_Pe.(t,Δω,g)
Pe2 = TLSInfer.model_Pe_relax.(t,Δω,g,T₁)
Pe3 = TLSInfer.model_Pe_exact.(t,Δω,g,Γ,T₁)

plot(t, [Pe1 Pe2 Pe3])