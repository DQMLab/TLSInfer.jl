using AlfredClient, TLSInfer, QuAnalysis, Statistics, StatsBase, DelimitedFiles, PyPlot, Dates

job = AlfredClient.get_job(1400)

function plot_particles(X, W, fignum=1)
    f = figure(fignum, figsize=(8,8)); f.clf()
    plot(f0,g0,"ro")
    scatter(X[1,:]./2π,1E3.*X[2,:]./2π,c=W,alpha=7/8,s=1,cmap="coolwarm")
    xlim(f0-0.0005, f0+0.0005); ylim(g0-0.05, g0+0.05);
    xlabel("TLS Frequency (GHz)"); ylabel("Coupling Strength (MHz)")
    grid(true); tight_layout(); display(f)
    return nothing
end


# true vals
f0 = 4.83008074; g0 = 1.44522;

# data
nParticles = 40_000
tm = 1.5E3 # ns
vis = 0.92493635
measurement_shots = 786
T₁ = 14_000
M = job["data"]["M"]
ZFreqs = job["data"]["ZFreqs"]*1E-9
ZTime = job["data"]["ZTime"]*1E9
e_count = round.(Int,job["data"]["Pe"].*measurement_shots)
tlsFreqInit = job["data"]["tlsFreqInit"][:,1]
tlsCoupInit = job["data"]["tlsCoupInit"][:,1]

# last iteration
fTLSfinal = M[1,1,end,:]
gTLSfinal = M[2,1,end,:]
W = zeros(nParticles)
X = zeros(2,nParticles)
Y = copy(X)
for i in 1:1000 #[133, 134, 705, 706, 712, 713]
    println("\n$i:")
    display(M[:,:,end,i])
    # particle distribution
    X[1,:] .= 2π .* (M[1,1,end,i] .+ M[1,2,end,i] .* randn(nParticles)) # particles_f
    X[2,:] .= 2π .* (M[2,1,end,i] .+ M[2,2,end,i] .* randn(nParticles)) # particles_g
    
    # calc likelihood
    ν,t,c = ZFreqs[end,i], ZTime[end,i], e_count[end,i]
    TLSInfer.particle_weights!(W,X,ν,t,c,measurement_shots,vis,T₁,tm)
    #plot_particles(X,W)
    
    # resample
    TLSInfer.resample_particles!(Y,X,W)
    μ = vec(mean(Y,dims=2))
    σ = sqrt.(abs.(diag(Y*Y'./nParticles .- μ*μ')))
    display([μ σ] ./ 2π)
    fTLSfinal[i] = μ[1] / 2π
    gTLSfinal[i] = μ[2] / 2π
end


#=
using BenchmarkTools

@benchmark TLSInfer.particle_weights!($W,$X,ν,t,c,$measurement_shots,$vis,$T₁,$tm) setup =
(i=rand(1:1000); (ν,t,c) = (ZFreqs[end,i], ZTime[end,i], e_count[end,i]); X[1,:] .= 2π .* (M[1,1,end,i] .+ M[1,2,end,i] .* randn(nParticles)); X[2,:] .= 2π .* (M[2,1,end,i] .+ M[2,2,end,i] .* randn(nParticles)))

@benchmark TLSInfer.resample_particles!($Y,$X,$W)
=#

function movmean(A::AbstractVector, n::Integer)
    cs = cumsum(A); pushfirst!(cs, 0)
    return [(cs[i]-cs[i-n])/n for i = n+1:length(cs)]
end

job1496 = AlfredClient.get_job(1496)

let fig = figure(2, figsize=(8,6)); fig.clf();
    frqs = (fTLSfinal .- mean(fTLSfinal)).*1E3
    tt = (22.8/60).*(0:length(frqs)-1)
    tt = tt[abs.(frqs).<0.6]
    movfrqs = movmean(frqs[abs.(frqs).<0.6],8);
    tt = tt[8:end]
    #
    fq = vec(job1496["data"]["CenterFreqs"]).*1E-6
    n_freqs = length(fq)
    fR = reduce(hcat, [QuAnalysis.fitRamsey(job1496,:,i)[1][:,4] for i in 1:n_freqs])
    Δf_data = (job1496["data"]["Ramsey"]["Detuning"] .- fR) .* 1E-6
    Δf_data = Δf_data .- mean(Δf_data, dims=1)
    Δt = Dates.value.(job1496["data"]["Ramsey"]["TimeStart"]) .* 1E-3/60
    Δt .-= Δt[1]
    Δt, Δf_data = vec(permutedims(Δt)), vec(permutedims(Δf_data))
    Δt, Δf_data = Δt[705:1450], movmean(Δf_data[701:1450],5)
    Δt .-= Δt[1]
    #
    plot(tt, movfrqs); @show std(movfrqs)
    plot(Δt, Δf_data); @show std(Δf_data)
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Time (min)"); ylabel("Freq (MHz)"); tight_layout(); display(fig);
end

# try to recreate track #j
for j = 1:300
    display(M[:,:,1,j])
    X[1,:] .= 2π .* (tlsFreqInit[j] .+ 0.0150 .* (rand(nParticles).-0.5)) # particles_f
    X[2,:] .= 2π .* (tlsCoupInit[j] .+ 0.0025 .* (rand(nParticles).-0.5)) # particles_g
    μ = vec(mean(X,dims=2))
    σ = sqrt.(abs.(diag(X*X'./nParticles .- μ*μ')))
    display([μ σ] ./ 2π)
    M_sim = zeros(2,2,35,1000)
    for i in 1:35
        #println("Iteration $i")
        
        # calc likelihood
        ν,t = ZFreqs[i,j], ZTime[i,j]
        TLSInfer.particle_weights!(W,X,ν,t,e_count[i,j],measurement_shots,vis,T₁,tm)
        #plot_particles(X,W)
        
        # resample
        TLSInfer.resample_particles!(Y,X,W)
        μ = vec(mean(Y,dims=2))
        σ = sqrt.(abs.(diag(Y*Y'./nParticles .- μ*μ')))
        #display([μ σ] ./ 2π)
        X,Y = Y,X
        M_sim[:,:,i,j] = [μ σ] ./ 2π
    end
    # plot original & simulated track
    fig = figure(3, figsize=(8,8)); fig.clf();
    
    freq = [M[1,1,:,j]; fTLSfinal[j]]
    err = [M[1,2,:,j]; 0]
    plot(freq, 0:35, label="original");
    fill_betweenx(0:35, freq.-err, freq.+err, alpha=0.2)
    
    freq = [tlsFreqInit[j]; M_sim[1,1,:,j]]
    err = [0.0043; M_sim[1,2,:,j]]
    plot(freq, 0:35, label="simulated");
    fill_betweenx(0:35, freq.-err, freq.+err, alpha=0.2)
    
    xlim(4.825,4.835); ylim(0,35);
    xlabel("TLS Frequency (GHz)"); ylabel("Iteration"); legend();
    grid(true); tight_layout(); display(fig);
end

# scatter plot of final results
let fig = figure(4, figsize=(6,6)); fig.clf();
    scatter(M[1,1,end,:],M[2,1,end,:]*1e3,s=10)#hypot.(M[1,2,end,:],M[2,2,end,:]).*1E5);# colorbar();
    xlim(f0-0.001, f0+0.001); ylim(g0-0.1, g0+0.1); grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Coupling (MHz)"); tight_layout(); display(fig);
end


