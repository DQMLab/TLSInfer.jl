using PyPlot, DelimitedFiles
const PLOT_DIR = joinpath(@__DIR__,"data")

function heatmap(x,y,Z;lim=nothing,ar=3/2,xlabel=nothing,ylabel=nothing,cmap="viridis")
    Δx, Δy = x[2]-x[1], y[2]-y[1]
    vmin, vmax = (lim == nothing ? extrema(Z) : lim)
    R = (x[end]-x[1])/(y[end]-y[1])
    clf()
    imshow(Z,extent=[x[1]-Δx/2,x[end]+Δx/2,y[1]-Δy/2,y[end]+Δy/2],
        origin="lower",vmin=vmin,vmax=vmax,aspect=R/ar,cmap=cmap)
    colorbar()
    xlabel == nothing || PyPlot.xlabel(xlabel)
    ylabel == nothing || PyPlot.ylabel(ylabel)
    tight_layout()
    return gcf()
end

function plot_particles(X,W,ν,t,s,Δt,e_count,M,l)
    clf()
    size(X,1) == 4 && gcf().add_subplot(1,2,1)
    scatter(X[1,:]./2π,1E3.*X[2,:]./2π,c=W,alpha=7/8,s=1,cmap="coolwarm")
    xlim([4.997, 5.003]); ylim([0.1, 0.5]);
    xlabel("TLS Frequency (GHz)"); ylabel("Coupling Strength (MHz)")
    grid(true); tight_layout()
    if size(X,1) == 4
        gcf().add_subplot(1,2,2)
        scatter(X[3,:],X[4,:],c=W,alpha=7/8,s=1,cmap="coolwarm")
        xlim([0, 100]); ylim([0, 100]); grid(true)
    end
    savefig(joinpath(PLOT_DIR, "frame_$l.png"),dpi=600,transparent=false)
    writedlm(joinpath(PLOT_DIR, "dist_$l.dat"), Float32.(permutedims(X)/2pi))
    writedlm(joinpath(PLOT_DIR, "weights_$l.dat"), Float32.(W))
    sleep(0.1)
    return nothing
end

function plot_likelihood(X,W,ν,t,s,Δt,e_count,M,l)
    f = 4.997 : 0.00001 : 5.003
    g = 0.00995 : 0.0000002 : 0.01005
    L1 = tls.ℒ.(2π.*f',2π.*g,ν,t,e_count,M,1,20000,t)
    if size(X,1) == 4
        λ = 0 : 0.5 : 100
        μ = 0 : 0.5 : 100
        L2 = tls.tls_presence_prob.(s,Δt,λ',μ)
    end
    clf()
    size(X,1) == 4 && gcf().add_subplot(1,2,1)
    heatmap(f,g,L1,xlabel="TLS Frequency (GHz)",ylabel="Coupling Strength (MHz)")
    if size(X,1) == 4
        gcf().add_subplot(1,2,2)
        imshow(L2,extent=[λ[1]-step(λ)/2,λ[end]+step(λ)/2,μ[1]-step(μ)/2,μ[end]+step(μ)/2], origin="lower",vmin=0.0,vmax=0.10,aspect=1,cmap="coolwarm")
        colorbar()
    end
    sleep(1)
    return nothing
end

function plot_model(modelfun::T) where {T<:Function}
    ν = permutedims(4.995 : 0.00001 : 5.005)
    t = 0.0 : 1 : 1000
    L = modelfun.(t,ν)
    heatmap(ν,t,L,lim=(0,1),xlabel="Probe Frequency (GHz)",ylabel="Interaction Time (ns)")
    return nothing
end
