Pkg.activate(dirname(@__DIR__))
using AlfredClient, TLSInfer, Statistics, DelimitedFiles, PyPlot, Serialization

# load data
particles = deserialize(joinpath(@__DIR__, "estimation_data", "particles.jls"))
ZFreqs = readdlm(joinpath(@__DIR__,"estimation_data","estimation_zfreqs.csv"), ',')
ZTime = readdlm(joinpath(@__DIR__,"estimation_data","estimation_ztime.csv"), ',')
e_count = readdlm(joinpath(@__DIR__,"estimation_data","estimation_e_count.csv"), ',')

tm = 1.5E3 # ns
vis = 0.93
mshots = 786
T₁ = 14_000

function run_estimations((particles_f,particles_g), zfreq, ztime, e_count, mshots, vis, T₁, tm)
    n_particles = length(particles_f)
    n_runs = 200
    W = zeros(n_particles)
    X = zeros(2,n_particles)
    Y = copy(X)
    M_sim = zeros(2,2,35,n_runs)
    for j in 1:n_runs
        X[1,:] .= particles_f.*(2π*1E-9)
        X[2,:] .= particles_g.*(2π*1E-9)
        #μ = vec(mean(X,dims=2))
        #σ = sqrt.(abs.(diag(X*X'./n_particles .- μ*μ')))
        #display([μ σ] ./ 2π)
        for i in 1:35
            #println("Iteration $i")
            # calc likelihood
            ν,t = zfreq[i], ztime[i]
            TLSInfer.particle_weights!(W,X,ν,t,e_count[i],mshots,vis,T₁,tm)
            # resample
            TLSInfer.resample_particles!(Y,X,W)
            μ = vec(mean(Y,dims=2))
            σ = sqrt.(abs.(diag(Y*Y'./n_particles .- μ*μ')))
            #display([μ σ] ./ 2π)
            M_sim[:,:,i,j] = [μ σ] ./ 2π
            X,Y = Y,X
        end
        #display([μ σ] ./ 2π)
    end
    return M_sim
end

@time for i = 1:1
    local M_sim
    M_sim = run_estimations(particles[i], ZFreqs[:,i], ZTime[:,i], e_count[:,i], mshots, vis, T₁, tm)
    display(dropdims(mean(M_sim[:,:,end,:],dims=3),dims=3).*1e3)
    display(dropdims(std(M_sim[:,:,end,:],dims=3),dims=3).*1e3)
end

#f=figure(1); f.clf(); hist(M_sim[1,1,end,:]); display(f);
#f=figure(2); f.clf(); hist(M_sim[2,1,end,:]); display(f);


#=
bin_info = readdlm(joinpath(@__DIR__,"octave_data","bin_info.csv"),',',Float32)
bin_Pe   = readdlm(joinpath(@__DIR__,"octave_data","bin_Pe.csv"),',',Float32)

bin_P = 1 .- vec(mean(bin_Pe,dims=2))
peak_prominence = 0.2
sampling_threshold = 0.2
n_particles = 40_000

using TLSInfer.OctaveAnalysis
#particles = OctaveAnalysis.gen_particles(n_particles, sampling_threshold, bin_info, bin_P, peak_prominence)
#serialize(joinpath(@__DIR__,"estimation_data","particles.jls"),particles)

job_1385 = AlfredClient.get_job(1385) # RM1, RM2, RM3
job_1394 = AlfredClient.get_job(1394) # RM1, RM3
job_1397 = AlfredClient.get_job(1397) # RM2, RM3
job_1399 = AlfredClient.get_job(1399) # RM2, RM3
job_1402 = AlfredClient.get_job(1402) # RM1, RM2, RM3
job_1405 = AlfredClient.get_job(1405) # RM1, RM2, RM3

ZFreqs =      [job_1394["data"]["ZFreqs"][:,1] job_1405["data"]["ZFreqs"][:,2] job_1399["data"]["ZFreqs"][:,2]].*1E-9
ZTime =         [job_1394["data"]["ZTime"][:,1] job_1405["data"]["ZTime"][:,2] job_1399["data"]["ZTime"][:,2]].*1E9
e_count = round.(Int, [job_1394["data"]["Pe"][:,1] job_1405["data"]["Pe"][:,2] job_1399["data"]["Pe"][:,2]].*mshots)
=#
