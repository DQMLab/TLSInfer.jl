using LsqFit, PyPlot, AlfredClient, QuAnalysis

# import data
jobIDs = [1179, 1209, 1298]

data = map(jobIDs) do jobID
    job = AlfredClient.get_job(jobID)
    jobID |> display
    #job["timeStamps"] |> display
    for l in eachline(IOBuffer(job["mFiles"]["file"][4]))
        #occursin("zVoltage =", l) && println(l)
        #occursin("zPulseP = [0", l) && println(l)
        occursin("qubitFreq = ", l) && println(l)
        occursin("qbXY4.CarrierPower", l) && println(l)
        occursin("fsw2.Frequency =", l) && println(l)
        occursin("fsw2.Power =", l) && println(l)
    end
    job["data"]
end

freqs_1179 = 1E3.*[4.8295563200456,4.82982004080458,4.83008365551522,4.83034716418462,4.83061056681985,4.83087386342799,4.83113705401611,4.83140013859128,4.83166311716056,4.831925989731,4.83218875630966,4.83245141690359,4.83271397151983,4.83297642016541,4.83323876284738,4.83350099957275,4.83376313034855,4.83402515518179,4.8342870740795,4.83454888704868,4.83481059409634,4.83507219522947,4.83533369045506,4.83559507978011,4.83585636321161,4.83611754075653,4.83637861242184,4.83663957821452,4.83690043814154,4.83716119220985,4.83742184042641,4.83768238279817,4.83794281933207,4.83820315003507,4.8384633749141,4.83872349397608,4.83898350722795,4.83924341467662,4.83950321632903,4.83976291219207,4.84002250227266,4.84028198657771,4.8405413651141,4.84080063788875,4.84105980490852,4.84131886618032,4.84157782171101,4.84183667150748,4.8420954155766,4.84235405392523,4.84261258656023,4.84287101348846,4.84312933471677,4.84338755025201,4.84364566010102,4.84390366427063,4.84416156276769,4.84441935559902,4.84467704277144,4.84493462429177,4.84519210016683,4.84544947040343,4.84570673500836,4.84596389398844,4.84622094735045,4.84647789510119,4.84673473724744,4.84699147379599,4.84724810475361,4.84750463012707,4.84776104992315,4.8480173641486,4.84827357281018,4.84852967591465,4.84878567346875,4.84904156547923,4.84929735195284,4.84955303289629,4.84980860831633,4.85006407821968,4.85031944261307,4.8505747015032,4.85082985489679,4.85108490280055,4.85133984522118,4.85159468216538,4.85184941363983,4.85210403965124,4.85235856020629,4.85261297531165,4.852867284974,4.85312148920001,4.85337558799635,4.85362958136967,4.85388346932664,4.85413725187391,4.85439092901811,4.85464450076591,4.85489796712393,4.85515132809881,4.85540458369718,4.85565773392567,4.85591077879088,4.85616371829945,4.85641655245798,4.85666928127307,4.85692190475134,4.85717442289937,4.85742683572375,4.85767914323109,4.85793134542796,4.85818344232094,4.8584354339166,4.85868732022152,4.85893910124226,4.85919077698539,4.85944234745745,4.859693812665,4.8599451726146,4.86019642731277,4.86044757676607]
freqs_1209 = 1E3.*[4.80629030207016,4.80656324371464,4.80683607867768,4.80710880696681,4.80738142858953,4.80765394355335,4.80792635186578,4.80819865353432,4.80847084856645,4.80874293696968,4.80901491875147,4.80928679391932,4.80955856248068,4.80983022444304,4.81010177981385,4.81037322860057,4.81064457081065,4.81091580645155,4.8111869355307,4.81145795805553,4.81172887403349,4.81199968347201,4.81227038637849,4.81254098276036,4.81281147262503,4.81308185597991,4.81335213283241,4.8136223031899,4.8138923670598,4.81416232444948,4.81443217536633,4.81470191981772,4.81497155781102,4.81524108935361,4.81551051445284,4.81577983311606,4.81604904535064,4.81631815116391,4.81658715056322,4.81685604355591,4.8171248301493,4.81739351035072,4.8176620841675,4.81793055160694,4.81819891267637,4.81846716738308,4.81873531573438,4.81900335773756,4.81927129339992,4.81953912272874,4.81980684573131,4.82007446241489,4.82034197278676,4.82060937685419,4.82087667462444,4.82114386610476,4.82141095130241,4.82167793022463,4.82194480287867,4.82221156927175,4.82247822941112,4.822744783304,4.82301123095761,4.82327757237916,4.82354380757588,4.82380993655496,4.82407595932361,4.82434187588902,4.82460768625839,4.82487339043891,4.82513898843775,4.82540448026209,4.82566986591911,4.82593514541598,4.82620031875985,4.82646538595789,4.82673034701724,4.82699520194506,4.82725995074849,4.82752459343466,4.82778913001071,4.82805356048377,4.82831788486096,4.82858210314941,4.82884621535621,4.82911022148849,4.82937412155334,4.82963791555787,4.82990160350916,4.83016518541432,4.83042866128042,4.83069203111454,4.83095529492376,4.83121845271515,4.83148150449577,4.83174445027269,4.83200729005295,4.83227002384361,4.83253265165172,4.83279517348431,4.83305758934843,4.8333198992511,4.83358210319935,4.8338442012002,4.83410619326067,4.83436807938777,4.83462985958851,4.83489153386989,4.8351531022389,4.83541456470255,4.83567592126781,4.83593717194168,4.83619831673113,4.83645935564313,4.83672028868465,4.83698111586267,4.83724183718412,4.83750245265598,4.83776296228519,4.8380233660787,4.83828366404344]
freqs_1298 = 1E3.*[4.78386390084487,4.78428625197365,4.78470836167101,4.78513022996375,4.78555185687865,4.78597324244246,4.7863943866819,4.78681528962367,4.78723595129443,4.78765637172083,4.78807655092947,4.78849648894695,4.78891618579981,4.7893356415146,4.78975485611782,4.79017382963594,4.79059256209541,4.79101105352266,4.79142930394408,4.79184731338605,4.7922650818749,4.79268260943694,4.79309989609847,4.79351694188575,4.79393374682501,4.79435031094246,4.79476663426427,4.79518271681661,4.7955985586256,4.79601415971733,4.79642952011789,4.79684463985332,4.79725951894964,4.79767415743284,4.79808855532889,4.79850271266373,4.79891662946327,4.79933030575341,4.79974374155999,4.80015693690886,4.80056989182582,4.80098260633666,4.80139508046712,4.80180731424294,4.80221930768982,4.80263106083343,4.80304257369943,4.80345384631344,4.80386487870105,4.80427567088783,4.80468622289933,4.80509653476107,4.80550660649854,4.8059164381372,4.8063260297025,4.80673538121985,4.80714449271464,4.80755336421223,4.80796199573795,4.80837038731711,4.80877853897501,4.80918645073689,4.80959412262798,4.81000155467351,4.81040874689864,4.81081569932853,4.8112224119883,4.81162888490307,4.81203511809791,4.81244111159787,4.81284686542798,4.81325237961323,4.81365765417861,4.81406268914905,4.81446748454949,4.81487204040482,4.81527635673992,4.81568043357962,4.81608427094876,4.81648786887213,4.8168912273745,4.81729434648061,4.81769722621518,4.81809986660291,4.81850226766847,4.8189044294365,4.81930635193162,4.81970803517842,4.82010947920148,4.82051068402532,4.82091164967447,4.82131237617343,4.82171286354666,4.8221131118186,4.82251312101367,4.82291289115626,4.82331242227074,4.82371171438144,4.8241107675127,4.82450958168879,4.82490815693399,4.82530649327254,4.82570459072865,4.82610244932652,4.82650006909031,4.82689745004417,4.82729459221222,4.82769149561855,4.82808816028722,4.82848458624228,4.82888077350776,4.82927672210763,4.82967243206588,4.83006790340645,4.83046313615326,4.83085813033021,4.83125288596116,4.83164740306996,4.83204168168043,4.83243572181638,4.83282952350158,4.83322308675977,4.83361641161468,4.83400949809001,4.83440234620944,4.83479495599661,4.83518732747515,4.83557946066867,4.83597135560075,4.83636301229493,4.83675443077476,4.83714561106372,4.83753655318532,4.83792725716299,4.83831772302019,4.83870795078031,4.83909794046674,4.83948769210285,4.83987720571196,4.8402664813174,4.84065551894245,4.84104431861037,4.84143288034441,4.84182120416779,4.8422092901037,4.8425971381753,4.84298474840574,4.84337212081815,4.84375925543563,4.84414615228124,4.84453281137803,4.84491923274904,4.84530541641727,4.8456913624057,4.84607707073727,4.84646254143493,4.84684777452158,4.8472327700201,4.84761752795337,4.8480020483442,4.84838633121543,4.84877037658984,4.84915418449019,4.84953775493923,4.84992108795968,4.85030418357424,4.85068704180558,4.85106966267635,4.85145204620917,4.85183419242665,4.85221610135137,4.85259777300589,4.85297920741273,4.85336040459442,4.85374136457343,4.85412208737223,4.85450257301326,4.85488282151893,4.85526283291165,4.85564260721378,4.85602214444766]

function model_resonance(f,t,fR,g,T₁,A,B,tm)
    # dynamics of qubit with relaxation time T₁ interacting with a mode at frequency fR
    # fR: mode resonance frequency (natural freq)
    # g:  coupling strength (natural freq)
    # T₁: qubit relaxation time
    # A:  signal amplitude
    # B:  offset, should be close to 0
    # tm is the measurement time, which was fixed to avoid changing the readout pulse
    Δ = 2π*(f - fR)
    g = 2π*g
    #
    Ω = √(Δ^2 + 4g^2)
    P = (Ω+Δ)^2/4Ω^2*exp(-(Ω+Δ)*t/(2Ω*T₁)) + (Ω-Δ)^2/4Ω^2*exp(-(Ω-Δ)*t/(2Ω*T₁)) + 2*(g/Ω)^2*exp(-t/2T₁)*cos(Ω*t)
    #
    return A*P*exp(-(tm-t)/T₁) + B
end

function residuals!(F::Vector, p::Vector, fs::Vector, ts::Vector, Pes::Vector, T₁, tm)
    @inbounds fR,g,A,B = p
    @inbounds for i in 1:length(Pes)
        F[i] = model_resonance(fs[i],ts[i],fR,g,T₁,A,B,tm) - Pes[i]
    end
end

function fitmeasurements(freq::Array{T}, time::Array{T}, Pe::Array{T}; p0 = [4830, 1.41]) where T<:Real
    @assert size(freq) == size(time) == size(Pe)
    # starting coeffs passed in via p0, below are constants
    A = 0.83; B = 0.06; T₁ = 20; tm = 1.5+0.045
    # fit function
    vf = vec(freq); vt = vec(time); vPe = vec(Pe)
    f! = (F, p) -> residuals!(F, p, vf, vt, vPe, T₁, tm)
    # fit
    res = LsqFit.lmfit(f!, [p0; [A, B]], T[], vPe; autodiff = :forwarddiff)
    # print results
    println("\nFit Coefficients:"); display(coef(res))
    println("\nStandard Errors:"); display(stderror(res))
    #println("\nCI (±3σ):"); display(map(x->(x[2]-x[1])/2,confidence_interval(res,0.0027)))
    return res
end

# fit 1
time = repeat(vec(data[1]["ZTime"]).*1E6,outer=(1,121))
freq = repeat(freqs_1179',inner=(126,1))
Pe = data[1]["Pe"]
res = fitmeasurements(freq,time,Pe; p0 = [4846.5, 3.41]) # RM1

let fig = figure(1, figsize=(10,8)); fig.clf();
    fR,g,A,B = res.param; T₁ = 20; tm = 1.5+0.045
    f = range(4825,4855,length=201)
    t = range(0,1.5,length=201)
    scatter(vec(freq),vec(time),s=50,c=vec(Pe)); clim(0,1);
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end


# fit 2
time = repeat(vec(data[2]["ZTime"]).*1E6,outer=(1,121))
freq = repeat(freqs_1209',inner=(101,1))
Pe = data[2]["Pe"]
res = fitmeasurements(freq,time,Pe; p0 = [4822.5, 3.41]) # RM1

let fig = figure(2, figsize=(10,8)); fig.clf();
    fR,g,A,B = res.param; T₁ = 20; tm = 1.5+0.045
    f = range(4810,4835,length=201)
    t = range(0,1.5,length=201)
    scatter(vec(freq),vec(time),s=50,c=vec(Pe)); clim(0,1);
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end



# fit 3
time = repeat(vec(data[3]["ZTime"]).*1E6,outer=(1,54))
freq = repeat(freqs_1298[97:150]',inner=(81,1))
Pe = data[3]["Pe"][:,97:150]
res = fitmeasurements(freq,time,Pe; p0 = [4831.5, 1.11]) # RM1

let fig = figure(3, figsize=(10,8)); fig.clf();
    fR,g,A,B = res.param; T₁ = 20; tm = 1.5+0.045
    f = range(4810,4835,length=201)
    t = range(0,1.5,length=201)
    scatter(vec(freq),vec(time),s=50,c=vec(Pe)); clim(0,1);
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end


# fit 4
time = repeat(vec(data[3]["ZTime"]).*1E6,outer=(1,61))
freq = repeat(freqs_1298[38:98]',inner=(81,1))
Pe = data[3]["Pe"][:,38:98]
res = fitmeasurements(freq,time,Pe; p0 = [4810.1, 2.11]) # RM1

let fig = figure(4, figsize=(10,8)); fig.clf();
    fR,g,A,B = res.param; T₁ = 20; tm = 1.5+0.045
    f = range(4800,4820,length=201)
    t = range(0,1.5,length=201)
    scatter(vec(freq),vec(time),s=50,c=vec(Pe)); clim(0,1);
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end