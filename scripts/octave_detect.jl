Pkg.activate(dirname(@__DIR__))
using TLSInfer.OctaveAnalysis, DelimitedFiles, Random

bin_info = readdlm(joinpath(@__DIR__,"octave_data","bin_info.csv"),',',Float32)
bin_Pe   = readdlm(joinpath(@__DIR__,"octave_data","bin_Pe.csv"),',',Float32)

bin_P = 1 .- vec(mean(bin_Pe,dims=2))
#prominence = 0.50; OctaveAnalysis.detect_tls(bin_info, bin_P, prominence)

for prom in [0.75, 0.72, 0.50, 0.39, 0.09]
    OctaveAnalysis.detect_tls(bin_info, bin_P, prom)
end
