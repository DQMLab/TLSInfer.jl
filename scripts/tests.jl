using TLSInfer, BenchmarkTools
const tls = TLSInfer
include(joinpath(@__DIR__,"plotting.jl"))

let g = 0.001, T₁ = 10E3 # GHz, ns
    plot_model((t,ν)->tls.model_Pe_relax(t,2π*(ν-5),g*2π,T₁))
end

N = 40_000 # number of particles
X1,f1 = tls.gen_test_TLS(N,2π*5,2π*0.002,0.0,500,0.9,10E3)

tls.estimate_TLS_params(f1,copy(X1),20,cbfun=plot_particles)

Y = copy(X1)
W = normalize!(rand(size(X1,2)),1)
@benchmark tls.resample_particles!($Y,$X1,$W)



@benchmark tls.estimate_TLS_params(f,X,45,INFO=false) setup=((X,f)=tls.gen_test_TLS(N))


#=
function resample_particles!(Y::Matrix{T},X::Matrix{T},W::Vector{T}) where T<:Number
    d,N = size(X) # number of particles
    length(W) == N || throw(ArgumentError("weight vector length must equal the number of particles!"))
    size(X) == size(Y) || throw(ArgumentError("particle matrices must have the same size!"))
    # mean & covariance matrix of particles
    μ = vec(mean(X,dims=2))
    a = 0.98 # standard choice
    Σ = PDMat((1-a^2) .* (X*X'./N .- μ*μ') + 1E-12*Diagonal(μ)) # ensure positive-definiteness
    # create a distribution over the particle weights
    P = sampler(Categorical(W))
    D = FullNormal(copy(μ),Σ)
    Yₗ = copy(μ) # buffer
    # resample by drawing a particle and generating a new one from it
    for n = 1:N
        l = rand(P)
        unsafe_copyto!(D.μ,1,X,d*(l-1)+1,d)
        D.μ .= a.*D.μ .+ (1-a).*μ # set mean of distribution to draw from
        rand!(D,Yₗ) # draw a particle close to particle "l"
        unsafe_copyto!(Y,d*(l-1)+1,Yₗ,1,d)
    end
    return Y
end
=#
