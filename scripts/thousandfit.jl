using LsqFit, PyPlot, DelimitedFiles

# import data
data_dir = joinpath(@__DIR__,"thousandfit_data")
time = readdlm(joinpath(data_dir,"time.csv"), ',', Float64, dims=(35,1000)).*1E6
freq = readdlm(joinpath(data_dir,"freq.csv"), ',', Float64, dims=(35,1000)).*1E-6
Pe = readdlm(joinpath(data_dir,"Pe.csv"), ',', Float64, dims=(35,1000))

# plot points from all runs (35 × 1000)
let fig = figure(1, figsize=(12,9)); fig.clf();
    scatter(vec(freq),vec(time),s=1,c=vec(Pe)); colorbar();
    xlim(4820,4840); ylim(0,1.5); clim(0,1); grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end

function model_resonance(f,t,fR,g,T₁,A,B,tm)
    # dynamics of qubit with relaxation time T₁ interacting with a mode at frequency fR
    # fR: mode resonance frequency (natural freq)
    # g:  coupling strength (natural freq)
    # T₁: qubit relaxation time
    # A:  signal amplitude
    # B:  offset, should be close to 0
    # tm is the measurement time, which was fixed to avoid changing the readout pulse
    Δ = 2π*(f - fR)
    g = 2π*g
    #
    Ω = √(Δ^2 + 4g^2)
    P = (Ω+Δ)^2/4Ω^2*exp(-(Ω+Δ)*t/(2Ω*T₁)) + (Ω-Δ)^2/4Ω^2*exp(-(Ω-Δ)*t/(2Ω*T₁)) + 2*(g/Ω)^2*exp(-t/2T₁)*cos(Ω*t)
    #
    return A*P*exp(-(tm-t)/T₁) + B
end

function residuals!(F::Vector, p::Vector, fs::Vector, ts::Vector, Pes::Vector, tm)
    @inbounds fR,g,T₁,A,B = p
    @inbounds for i in 1:length(Pes)
        F[i] = model_resonance(fs[i],ts[i],fR,g,T₁,A,B,tm) - Pes[i]
    end
end

function fitthousand(freq::Array{T}, time::Array{T}, Pe::Array{T}) where T<:Real
    @assert size(freq) == size(time) == size(Pe)
    # starting coeffs
    p0 = [4830, 1.4, 16, 0.8, 0.1]; tm = 1.5+0.045
    # fit function
    vf = vec(freq); vt = vec(time); vPe = vec(Pe)
    f! = (F, p) -> residuals!(F, p, vf, vt, vPe, tm)
    # fit
    res = LsqFit.lmfit(f!, p0, T[], vPe; autodiff = :forwarddiff)
    # print results
    println("\nFit Coefficients:"); display(coef(res))
    println("\nStandard Errors:"); display(stderror(res))
    println("\nCI (±3σ):"); display(map(x->(x[2]-x[1])/2,confidence_interval(res,0.0027)))
    return res
end

# fit
res = fitthousand(freq,time,Pe)

let fig = figure(2, figsize=(12,9)); fig.clf();
    fR,g,T₁,A,B = res.param; tm = 1.5+0.045
    f = range(4820,4840,length=201)
    t = range(0,1.5,length=401)
    heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    scatter(vec(freq),vec(time),s=3,c=vec(Pe)); clim(0,1);
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end
