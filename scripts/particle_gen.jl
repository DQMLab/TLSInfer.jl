using TLSInfer.OctaveAnalysis, DelimitedFiles, PyPlot

bin_info = readdlm(joinpath(@__DIR__,"octave_data","bin_info.csv"),',',Float32)
bin_Pe   = readdlm(joinpath(@__DIR__,"octave_data","bin_Pe.csv"),',',Float32)

bin_P = 1 .- vec(mean(bin_Pe,dims=2))
peak_prominence = 0.10
sampling_threshold = 0.30
n_particles = 40_000

particles = OctaveAnalysis.gen_particles(n_particles, sampling_threshold, bin_info, bin_P, peak_prominence, false)

let fig = figure(1, figsize=(6,6)); fig.clf()
    for (particles_f, particles_g) in particles
        scatter(particles_f*1E-9, particles_g*1E-6, 0.25)
    end
    xlabel("\$f_{RM}\$ (GHz)"); ylabel("\$g\$ (MHz)");
    xlim(4.79,5.19); ylim(0,70);
    grid(true); tight_layout(); display(fig);
end
