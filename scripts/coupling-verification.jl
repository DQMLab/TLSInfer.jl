using LsqFit, PyPlot, AlfredClient, QuAnalysis

# import data
jobIDs = [1385, 1394, 1397, 1399, 1402, 1405, 1400] # 1400 is thousand data

#params, errors, _, _, detunings = QuAnalysis.fitRamsey(AlfredClient.get_job(1405))
#AlfredClient.get_job(1405)["data"]["Ramsey"]["CenterFreq"]-mean(params[:,4]-detunings)

idx_RM1 = [1, 1, 0, 0, 1, 0*1, 0]
idx_RM2 = [2, 0, 1, 1, 2, 2, 0]
idx_RM3 = [3, 2, 2, 2, 3, 3, 0]
idx = idx_RM1

data = mapreduce(hcat,zip(jobIDs,idx)) do (jobID,idx)
    job = AlfredClient.get_job(jobID)
    jobID |> display
    #job["timeStamps"] |> display
    for l in eachline(IOBuffer(job["mFiles"]["file"][jobID == 1400 ? 5 : 6]))
        #occursin("zVoltage =", l) && println(l)
        #occursin("zPulseP = [0", l) && println(l)
        occursin("tlsFreq =", l) && println(l)
        occursin("tlsPower =", l) && println(l)
        #occursin("qubitFreq =  4", l) && println(l)
        occursin("qbXY4.CarrierPower", l) && println(l)
    end
    if idx == 0
        zeros(40,0,3)
    else
        cat(job["data"]["ZFreqs"][:,idx].*1E-6,
            job["data"]["ZTime"][:,idx].*1E6,
            job["data"]["Pe"][:,idx], dims=3)
    end
end

time = data[:,:,2]
freq = data[:,:,1]
Pe = data[:,:,3]

# plot points from all runs (35 × 1000)
#let fig = figure(1, figsize=(12,9)); fig.clf();
#    scatter(vec(freq),vec(time),s=10,c=vec(Pe)); colorbar();
#    clim(0,1); grid(true,which="major",alpha=0.5,ls="-");
#    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
#end

function model_resonance(f,t,fR,g,T₁,A,B,tm)
    # dynamics of qubit with relaxation time T₁ interacting with a mode at frequency fR
    # fR: mode resonance frequency (natural freq)
    # g:  coupling strength (natural freq)
    # T₁: qubit relaxation time
    # A:  signal amplitude
    # B:  offset, should be close to 0
    # tm is the measurement time, which was fixed to avoid changing the readout pulse
    Δ = 2π*(f - fR)
    g = 2π*g
    #
    Ω = √(Δ^2 + 4g^2)
    P = (Ω+Δ)^2/4Ω^2*exp(-(Ω+Δ)*t/(2Ω*T₁)) + (Ω-Δ)^2/4Ω^2*exp(-(Ω-Δ)*t/(2Ω*T₁)) + 2*(g/Ω)^2*exp(-t/2T₁)*cos(Ω*t)
    #
    return A*P*exp(-(tm-t)/T₁) + B
end

function residuals!(F::Vector, p::Vector, fs::Vector, ts::Vector, Pes::Vector, T₁, A, B, tm)
    @inbounds fR,g = p
    @inbounds for i in 1:length(Pes)
        F[i] = model_resonance(fs[i],ts[i],fR,g,T₁,A,B,tm) - Pes[i]
    end
end

function fitmeasurements(freq::Array{T}, time::Array{T}, Pe::Array{T}; p0 = [4830, 1.41]) where T<:Real
    @assert size(freq) == size(time) == size(Pe)
    # starting coeffs passed in via p0, below are constants
    A = 0.83; B = 0.06; T₁ = 20; tm = 1.5+0.045
    # fit function
    vf = vec(freq); vt = vec(time); vPe = vec(Pe)
    f! = (F, p) -> residuals!(F, p, vf, vt, vPe, T₁, A, B, tm)
    # fit
    res = LsqFit.lmfit(f!, p0, T[], vPe; autodiff = :forwarddiff)
    # print results
    println("\nFit Coefficients:"); display(coef(res))
    println("\nStandard Errors:"); display(stderror(res))
    #println("\nCI (±3σ):"); display(map(x->(x[2]-x[1])/2,confidence_interval(res,0.0027)))
    return res
end

# fit
res = fitmeasurements(freq,time,Pe; p0 = [4810, 2.8]) # RM1
#res = fitmeasurements(freq,time,Pe; p0 = [4830, 1.41]) # RM2


let fig = figure(2, figsize=(10,8)); fig.clf();
    fR,g = res.param;  A = 0.83; B = 0.06; T₁ = 20; tm = 1.5+0.045
    f = range(4800,4820,length=201)
    t = range(0,1.5,length=201)
    scatter(vec(freq),vec(time),s=50,c=vec(Pe)); clim(0,1);
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,T₁,A,B,tm),lim=[0,1]);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end

#=
gtest = range(1.1,1.9,length=41)
gres = [fitmeasurements(freq,time,Pe,p0=[4830,g]) for g in gtest]
gfit = [coef(res)[2] for res in gres]
gerr = [stderror(res)[2] for res in gres]
let fig = figure(3, figsize=(10,8)); fig.clf();
    plot(gfit, gerr)
    tight_layout(); display(fig);
end
=#
