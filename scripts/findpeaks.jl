Pkg.activate(dirname(@__DIR__))
using TLSInfer.OctaveAnalysis, DelimitedFiles, PyPlot

bin_info = readdlm(joinpath(@__DIR__,"octave_data","bin_info.csv"),',',Float32)
bin_Pe   = readdlm(joinpath(@__DIR__,"octave_data","bin_Pe.csv"),',',Float32)

bins = 256:511 # last octave

bin_P = 1 .- vec(mean(bin_Pe[bins],dims=2))
freq = Float32.((bin_info[bins,2] .+ bin_info[bins,1])./2E9)

# find peaks
locs,pks = OctaveAnalysis.findpeaks(bin_P, 0.09, ends=true)


let fig = figure(1); fig.clf();
    plot(freq,bin_P);
    scatter(freq[locs], pks, color="red");
    xlim(freq[1],freq[end]); ylim(0,1);
    grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("1 - Pₑ"); tight_layout(); display(fig);
end


using PGFPlotsX
x = @pgf Axis( {xmin=freq[1], xmax=freq[end], ymin=0, ymax=1},
    Plot({no_markers}, Table([freq, bin_P])),
    Plot({scatter, only_marks, draw="red"}, Table([freq[locs], pks]))
    )
pgfsave("findpeaks.pgf",x)
