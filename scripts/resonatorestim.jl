using LsqFit, PyPlot, AlfredClient, QuAnalysis

# import data
jobIDs = [1385, 1394, 1397, 1399, 1402, 1405]
data = mapreduce(hcat,jobIDs) do jobID
    job = AlfredClient.get_job(jobID)
    cat(job["data"]["ZFreqs"][:,end].*1E-6,
        job["data"]["ZTime"][:,end].*1E6,
        job["data"]["Pe"][:,end], dims=3)
end

time = data[:,:,2]
idx = time .< 0.6
time = time[idx]
freq = data[:,:,1][idx]
Pe = data[:,:,3][idx]

# plot points from all runs (35 × 1000)
let fig = figure(1, figsize=(12,9)); fig.clf();
    scatter(vec(freq),vec(time),s=1,c=vec(Pe)); colorbar();
    xlim(5000,5100); ylim(0,0.6); clim(0,1); grid(true,which="major",alpha=0.5,ls="-");
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end

function model_resonance(f,t,fR,g,κ,γ,A,B,tm)
    Δ = 2π*(f - fR)
    #
    Λ = √((γ+2im*Δ-κ)^2 - 16*g^2)
    Γ,Ω = (κ - γ + real(Λ))/2, imag(Λ)/2
    a = (Λ-(κ-γ)+2im*Δ)/2Λ
    b = (Λ+(κ-γ)-2im*Δ)/2Λ
    #
    return A*abs2(a*cis(-t*Ω/2)*exp(-t*(γ+Γ)/2) + b*cis(t*Ω/2)*exp(-t*(κ-Γ)/2))*exp(-(tm-t)/T₁) + B
end

function residuals!(F::Vector, p::Vector, fs::Vector, ts::Vector, Pes::Vector, tm::Real)
    # dynamics of qubit with relaxation time T₁ interacting with a mode at frequency fR
    # p[1] = fR, mode resonance frequency (natural freq)
    # p[2] = g, coupling strength (natural freq)
    # p[3] = T₁, qubit relaxation time
    # p[4] = A, signal amplitude
    # p[5] = B, offset, should be close to 0
    # tm is the measurement time, which was fixed to avoid changing the readout pulse
    @inbounds fR,g,κ,γ,A,B = p
    # convert g
    g = 2π*g
    @inbounds for i in 1:length(Pes)
        F[i] = model_resonance(fs[i],ts[i],fR,g,κ,γ,A,B,tm) - Pes[i]
    end
end

function fitthousand(freq::Array{T}, time::Array{T}, Pe::Array{T}) where T<:Real
    @assert size(freq) == size(time) == size(Pe)
    # starting coeffs
    p0 = [5040, 37, 1, 1/14, 0.80, 0.05]; tm = 1.5+0.045
    # fit function
    vf = vec(freq); vt = vec(time); vPe = vec(Pe)
    f! = (F, p) -> residuals!(F, p, vf, vt, vPe, tm)
    # fit
    res = LsqFit.lmfit(f!, p0, T[], vPe; autodiff = :finite)
    # print results
    println("\nFit Coefficients:"); display(coef(res))
    println("\nStandard Errors:"); display(stderror(res))
    println("\nCI (±3σ):"); display(map(x->(x[2]-x[1])/2,confidence_interval(res,0.0027)))
    return res
end

# fit
res = fitthousand(freq,time,Pe)

let fig = figure(2, figsize=(12,9)); fig.clf();
    fR,g,κ,γ,A,B = res.param
    fR,g,κ,γ,A,B = [5031, 37, 1, 1/14, 0.80, 0.05]
    g = 2π*g
    tm = 1.5+0.045
    f = range(5000,5140,length=201)
    t = range(0,0.6,length=401)
    QuAnalysis.heatmap(vec(f),vec(t),model_resonance.(f',t,fR,g,κ,γ,A,B,tm),lim=[0,1]);
    scatter(vec(freq),vec(time),s=1,c=vec(Pe)); clim(0,1);
    ylim(0,0.6)
    xlabel("Frequency (MHz)"); ylabel("Time (μs)"); tight_layout(); display(fig);
end
