using TLSInfer, BenchmarkTools
const tls = TLSInfer
include(joinpath(@__DIR__,"plotting.jl"))

let g = 0.0002, Γ = 0.010, T₁ = 10E3 # GHz, GHz, ns
    plot_model((t,ν)->tls.model_Pe_exact(t,2π*(ν-5),g*2π,Γ,T₁))
end

N = 40_000 # number of particles
X1,f1 = tls.gen_test_TLS(N,2π*5,2π*0.0003,0.004,500,0.9,10E3)

tls.estimate_TLS_params(f1,copy(X1),20,cbfun=plot_particles)
