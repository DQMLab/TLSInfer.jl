model_Pe(t,Δω,g) = (Ω = √(Δω^2+4g^2); 1 - (2g/Ω)^2 * sin(Ω*t/2)^2)

# model with qubit T₁ relaxation
function model_Pe_relax(t,Δω,g,T₁=10_000,tm=t)
    # tm is the measurement time, which can happen later than the evolution
    # time, in which case we need to account for some additional relaxation
    Ω = √(Δω^2 + 4g^2)
    a = (Ω + Δω)^2/4Ω^2*exp(-(Ω + Δω)*t/(2*Ω*T₁))
    b = (Ω - Δω)^2/4Ω^2*exp(-(Ω - Δω)*t/(2*Ω*T₁))
    c = 2*(g/Ω)^2*exp(-t/2T₁)*cos(Ω*t)
    return (a + b + c)*exp(-(tm-t)/T₁)
end

# exact single-excitation fully dissipative model where Γ is the TLS decoherence rate
function model_Pe_exact(t,Δω,g,Γ,T₁=10_000,tm=t)
    γ = 1/T₁
    B = √((γ+2im*Δω-Γ)^2 - 16*g^2)
    a = (B-(Γ-γ)+2im*Δω)
    b = (B+(Γ-γ)-2im*Δω)
    α = (a*exp(-t*B/4) + b*exp(t*B/4))*exp(-t*(γ+Γ)/4)/2B
    return abs2(α)*exp(-(tm-t)*γ)
end

function ℒ(ω,g,ν,t,e_count,M,vis,T₁,tm)
    Pe = model_Pe_relax(t,ω-2π*ν,g,T₁,tm)
    p  = (1-vis)/2 + vis*Pe
    return pdf(Binomial(M,p),e_count)
end

function ℒ(ω,g,Γ,ν,t,e_count,M,vis,T₁,tm)
    #@show ω,g,Γ,ν,t,e_count,M,vis,T₁,tm
    Pe = model_Pe_exact(t,ω-2π*ν,g,Γ,T₁,tm)
    #@show Pe
    p  = (1-vis)/2 + vis*Pe
    return pdf(Binomial(M,p),e_count)
end
