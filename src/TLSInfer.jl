module TLSInfer
# non-Markovian Hamiltonian parameter estimation using online experiment design
# with simultaneous Monte Carlo and Bayesian inference techniques

using Statistics, LinearAlgebra, Random, Printf
using Distributions, PDMats
#using Base.Threads: @threads, nthreads, threadid


include("model.jl")
include("sampling.jl")
include("simulation.jl")
include("experimental.jl")

# OctaveAnalysis submodule
include("octave/octaveanalysis.jl")

function choose_measurement_controls(μ,σ,l,tlim=5E3)
    a,c = 1.571, 5
    L₀ = 25
    # generate some random numbers
    r₁ = rand() - 0.5
    r₂ = rand()
    z  = randn()
    # choose the measurement parameters ν,t
    if l <= L₀
        ν = (μ[1] + r₁*μ[2])/2π
        t = r₂*tanh(a/(σ[2]*tlim))*tlim
    else
        ν = (μ[1] + c*r₁*σ[1])/2π # typo in thesis?
        t = 0.5*(1+r₂)*tanh(a/(σ[2]*tlim))*tlim
    end
    return ν,t
end

function estimate_TLS_params(measure::Function,X::Matrix{T},L=30;cbfun=nothing,INFO=true) where T<:Real
    P,N = size(X) # number of parameters and particles

    # init variables
    Y = copy(X); s = 1; Δt = 0.0
    W = Vector{T}(undef,N)
    INFO && println("Starting inference!")
    for l = 1:L
        # calculate mean and standard deviation of particles
        μ = vec(mean(X,dims=2))
        σ = sqrt.(abs.(diag(X*X'./N .- μ*μ')))
        # choose the measurement parameters ν,t
        ν,t = choose_measurement_controls(μ,σ,l)
        # measure
        e_count,M = measure(ν,t)
        if INFO
            @printf("%2d: ",l)
            @printf("̄ω = %.3f(%02.0f)GHz, ̄g = %6.3f(%04.0f)MHz, ",μ[1]/2π,σ[1]*1E3/2π,μ[2]*1E3/2π,σ[2]*1E6/2π)
            @printf("Pₑ(ν = %.3fGHz, t = %4.0fns) = %5.1f%%, ",ν,t,100*model_Pe(t,2π*(ν-5),2π*0.01))
            @printf("nₑ/M = %5.1f%%\n",100*e_count/M)
        end
        # update particle distribution by calculating likelihoods and resampling
        W = particle_weights!(W,X,ν,t,e_count,M,0.95,14000,t)
        #INFO && println("effective sample size: $(1/sum(abs2,W))")
        Y = resample_particles!(Y,X,W)
        X,Y = Y,X # posterior becomes prior for next round
        if cbfun !== nothing # run callback function
            # note that we don't care about what happens to Y & W in cbfun,
            # as long as they don't change size
            cbfun(Y,W,ν,t,s,Δt,e_count,M,l)
        end
    end
    INFO && println("done!\n")
    return vec(mean(X,dims=2))
end

end # module
