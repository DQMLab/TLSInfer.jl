"""
    particle_weights!(W,X,ν,t,e_count,M,...)

Compute the weights of the distribution `X` by computing the likelihood of
having obtained the measurement result for each particle, given the appropriate
measurement parameters.
"""
function particle_weights!(W::Vector,X::Matrix,ν,t,e_count,M,vis,T₁,tm)
    P,N = size(X)
    length(W) == N || throw(ArgumentError("weight vector length must equal the number of particles!"))
    # calculate the measurement likelihood given the particle locations
    if P == 2
        for n = 1:N
            @inbounds W[n] = ℒ(X[1,n],X[2,n],ν,t,e_count,M,vis,T₁,tm)
        end
    elseif P == 3
        for n = 1:N
            @inbounds W[n] = ℒ(X[1,n],X[2,n],X[3,n],ν,t,e_count,M,vis,T₁,tm)
        end
    end
    W_sum = sum(W)
    if W_sum == 0
        W .= 1/N # in case the measurement doesn't make sense
    else
        W ./= W_sum # otherwise simply normalize
    end
    return W
end

"""
    resample_particles!(Y,X,W)

Compute the posterior particle distribution `Y` based on the prior distribution
`X` and the likelihood of each particle in the prior `W`.

After the resampling, the weights of the new particles should be considered
uniform. This is because the new particles were drawn from the prior particles
according to their likelihood.
"""
function resample_particles!(Y::Matrix{T},X::Matrix{T},W::Vector{T}) where T<:Number
    d,N = size(X) # number of particles
    length(W) == N || throw(ArgumentError("weight vector length must equal the number of particles!"))
    size(X) == size(Y) || throw(ArgumentError("particle matrices must have the same size!"))
    # mean & covariance matrix of particles
    μ = vec(mean(X,dims=2))
    a = 0.98 # standard choice
    Σ = PDMat((1-a^2) .* (X*X'./N .- μ*μ') + 1E-12*Diagonal(μ)) # ensure positive-definiteness
    # create a distribution over the particle weights
    P = sampler(Categorical(W))
    D = FullNormal(copy(μ),Σ)
    y = copy(μ) # buffer
    # resample by drawing a particle and generating a new one from it
    for n = 1:N
        l = rand(P)
        unsafe_copyto!(D.μ,1,X,d*(l-1)+1,d)
        D.μ .= a.*D.μ .+ (1-a).*μ # set mean of the distribution to draw from
        rand!(D,y) # draw a particle close to particle "l"
        unsafe_copyto!(Y,d*(n-1)+1,y,1,d)
    end
    return Y
end
