function findpeaks(X::AbstractVector{T}, prominence::Real; ends::Bool=false) where T<:Real
    inds = sizehint!(Vector{Int}(undef,0),length(X)÷10)
    peaks = sizehint!(Vector{T}(undef,0),length(X)÷10)
    # keep track of a few values
    min_val = first(X)
    min_ind = firstindex(X)
    pk_val = first(X)
    pk_ind = firstindex(X)
    left_val = ends ? lessthan(T,first(X)-prominence) : first(X)
    left_ind = firstindex(X)
    # loop over array to find peaks
    for (i,val) in enumerate(X)
        if val < min_val
            # keep track of local minimum
            min_val = val
            min_ind = i
        end
        if val > pk_val || ((val-min_val) > prominence && min_val < left_val)
            # keep track of local maximum
            # the OR condition is used when there is a local prominent peak that is
            # lower than the current pk_val (which we want to detect)
            pk_val = val
            pk_ind = i
            # since we found a new peak, check if we can update left_val
            if min_val < left_val # min_ind < pk_ind
                left_val = min_val
                left_ind = min_ind
            end
        else # we are to the right of a peak, check prominence
            if (pk_val-val) > prominence && (pk_val-left_val) > prominence
                # found peak
                push!(peaks, pk_val)
                push!(inds, pk_ind)
                # reset
                min_val = val
                pk_val = val
                left_val = val
            end
        end
    end
    if ends && (pk_val-min_val) > prominence
        # there was an end peak which didn't statisfy the right-side criteria
        push!(peaks, pk_val)
        push!(inds, pk_ind)
    end
    # return peaks
    return inds, peaks
end

function lessthan(::Type{T}, x::Real) where T<:Real
    if T isa Integer
        return round(T, x-1)
    else
        return prevfloat(T(x))
    end
end
