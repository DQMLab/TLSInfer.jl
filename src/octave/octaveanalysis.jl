module OctaveAnalysis
using LinearAlgebra, Distributions, Printf

include("findpeaks.jl")

# bin_P is simply the inverted bin Pe, this is done in order to detect peaks (not dips)
# bin_P =  1 .- vec(mean(bin_Pe,dims=2))
# the peak prominence must be specified, and is in Pe units

function detect_tls(bin_info::Matrix{T}, bin_P::Vector, prominence::Real, info=true) where T
    # detect TLS in octave data by looking for peaks
    # return the lowest octave in which the peak is found

    pk_freqs = T[]
    pk_octas = Int[]
    pk_inds  = Int[]
    pk_coupg = T[]

    # find peaks in each octave, starting from the highest one
    depth = Int(log2(size(bin_info,1)+1)) # will throw if log2 doesn't return an integer
    for o = depth-1 : -1 : 2
        bins = 2^o : 2^(o+1)-1
        pk_data = bin_P[bins] # extract data for this octave

        # First pass: find peaks with prominence above threshold (skip octave if there's none)
        locs,_ = findpeaks(pk_data, prominence, ends=true)
        isempty(locs) && continue

        # Second pass: compare results with peaks from earlier octaves to see if they belong to the
        # same TLS and keep only one if they do overlap
        freqs = mean(bin_info[bins,1:2], dims=2)
        this_binwidth = bin_info[bins[1],2] - bin_info[bins[1],1]
        i = 1
        while i <= length(locs)
            # to see if peaks from previous octaves match, we check if frequencies are close
            freq = freqs[locs[i]]
            match = findall(pk_freq -> abs(pk_freq-freq) < this_binwidth*2/3, pk_freqs)
            if length(match)==1 && pk_data[locs[i]] < bin_P[pk_inds[match[]]]/2
                # throw out the current peak if it is less than half as tall as the match
                deleteat!(locs, i) # don't increment i
            else # otherwise get rid of peak(s) from previous octave
                deleteat!(pk_freqs, match)
                deleteat!(pk_octas, match)
                deleteat!(pk_inds,  match)
                deleteat!(pk_coupg, match)
                i += 1
            end
        end

        # save peak info
        append!(pk_freqs, freqs[locs])
        append!(pk_octas, fill(o,length(locs)))
        append!(pk_inds,  bins[locs])
        append!(pk_coupg, fill(this_binwidth/2,length(locs)))
    end

    I = sortperm(pk_freqs)
    pk_freqs = pk_freqs[I]
    pk_octas = pk_octas[I]
    pk_inds  = pk_inds[I]
    pk_coupg = pk_coupg[I]

    if info # Print summary
        println("\n===== Octave Analysis Results =====\n")
        println("Minimum prominence value: $prominence\n")
        println("Number of detected TLS: $(length(pk_freqs))\n")
        length(pk_freqs) > 1 && println("Frequencies of detected TLS:")
        for i = 1:length(pk_freqs)
            @printf("  %d: %1.3f GHz at Octave %d, g ∈ [%.0f, %.0f] MHz (idx: %d)\n", i, pk_freqs[i]*1E-9, pk_octas[i], pk_coupg[i]*0.5E-6, pk_coupg[i]*1E-6, pk_inds[i])
        end
    end

    return pk_freqs, pk_octas, pk_inds
end

function gen_particles(n_particles::Integer, sampling_threshold::Real, bin_info::Matrix{T}, bin_P::Vector, prominence::Real, info=true) where T
    # Split spectrum
    split_bin_info, split_bin_P, split_bin_weight = split_spectrum(bin_info, bin_P, prominence, info)

    # Generate particle distributions for each detected TLS
    particles = map(1:length(split_bin_info)) do i
        bin_info = split_bin_info[i]
        bin_weight = split_bin_weight[i]
        binP = split_bin_P[i]

        # Sample from bins above sampling_threshold according to their weight
        I = findall(>(sampling_threshold), binP)
        if isempty(I)
            error("No bins were found above the sampling threshold ($sampling_threshold), set a lower value!")
        end
        D = Categorical(normalize!(Float64.(binP[I].*bin_weight[I]),1)) # convert to Float64
        n = I[rand(D,n_particles)]

        # Create distribution
        particles_f = (bin_info[n,2].-bin_info[n,1]).*rand(T,n_particles) .+ bin_info[n,1]
        particles_g = (1 .+ rand(T,n_particles))./(bin_info[n,4] .- bin_info[n,3])./8
        return particles_f, particles_g
    end

    if info
        @printf("\nBin sampling threshold: %.2f\n", sampling_threshold)
        println("\nParticle distribution stats:")
        for i = 1:length(particles)
            @printf("  %d: ⟨f⟩ = %1.3f(%.0f) GHz; ⟨g⟩ = %.0f(%.0f)\n", i,
                mean(particles[i][1])*1E-9, std(particles[i][1])*1E-6,
                mean(particles[i][2])*1E-6, std(particles[i][2])*1E-6)
        end
    end

    return particles
end

function split_spectrum(bin_info::Matrix{T}, bin_P::Vector, prominence::Real, info=true) where T
    # split Octave data in order to have a single TLS per dataset (if any)

    # detect TLS
    pk_freqs, pk_octas = detect_tls(bin_info, bin_P, prominence, info)

    if length(pk_freqs) == 0
        error("No TLS detected!")
    elseif length(pk_freqs) == 1 # only one peak detected, no split needed
        split_bin_info = [bin_info]
        split_bin_P = [bin_P]
        split_bin_weight = [ones(T,size(bin_P))]
        return split_bin_info, split_bin_P, split_bin_weight
    end

    # Chop spectrum at weighted midpoint (according to octave bin width) of each peak
    # bins from a lower octave (larger width) will have a larger share of the spectrum
    full_BW = bin_info[1,2] - bin_info[1,1]
    pk_binwidth = full_BW ./ 2 .^ pk_octas
    weights = 1 ./ [pk_binwidth[1:end-1] pk_binwidth[2:end]]
    splits = sum([pk_freqs[1:end-1] pk_freqs[2:end]].*weights,dims=2) ./ sum(weights,dims=2)
    bounds = [bin_info[1,1]; splits; bin_info[1,2]] # frequencies at which we cut the spectrum

    split_bin_info = [similar(bin_info,0,4) for i in 1:length(pk_freqs)]
    split_bin_P = [similar(bin_P,0) for i in 1:length(pk_freqs)]
    split_bin_weight = [zeros(T,0) for i in 1:length(pk_freqs)]

    # Go octave by octave and chop bin_P & bin_info according to the frequency bounds we computed
    # if a bound falls whithin a bin, cut the bin in two and reduce its weight accordingly
    depth = Int(log2(size(bin_info,1)+1)) # will throw if log2 doesn't return an integer
    for o = 0:depth-1
        bins = 2^o : 2^(o+1)-1
        for i = 1:length(bounds)-1
            freqL = bounds[i]
            freqR = bounds[i+1]
            # Find edge bins in that octave
            # Find the last bin with left frequency less than or equal to the left edge
            binL = findlast(<=(freqL), bin_info[bins,1])
            # Find the first bin with right frequency larger than or equal to the right edge
            binR = findfirst(>=(freqR), bin_info[bins,2])
            # Grab bins
            this_bin_info = bin_info[bins[binL:binR],:]
            this_bin_weight = ones(T,size(this_bin_info,1)) #fill(one(T)/2^(2*o),size(this_bin_info,1))
            # Now, for the edge bins, check if the edge frequency falls inside the bin
            if freqL != this_bin_info[1,1] # then we need to cut the bin
                # Reduce the weight of that bin to be proportional to spectrum left over
                this_bin_weight[1] *= (this_bin_info[1,2]-freqL)/(this_bin_info[1,2]-this_bin_info[1,1])
                # Set the frequency to the edge freq
                this_bin_info[1,1] = freqL
            end
            if freqR != this_bin_info[end,2] # then we need to cut the bin
                # Reduce the weight of that bin
                this_bin_weight[end] *= (freqR-this_bin_info[end,1])/(this_bin_info[end,2]-this_bin_info[end,1])
                # Set the frequency to the edge freq
                this_bin_info[end,2] = freqR
            end
            # Save those bins in a per-TLS array (concatenating octaves)
            split_bin_info[i] = [split_bin_info[i]; this_bin_info]
            append!(split_bin_P[i], bin_P[bins[binL:binR]])
            append!(split_bin_weight[i], this_bin_weight)
        end
    end

    return split_bin_info, split_bin_P, split_bin_weight
end

end
