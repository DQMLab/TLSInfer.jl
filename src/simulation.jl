# simulated swap-spectroscopy measurement, including relaxation
function swap_spectroscopy(ν,t,ω=2π*5,g=2π*0.010,Γ=0.0,M=500,vis=0.95,T₁=14000)
    Pe = model_Pe_exact(t,ω-2π*ν,g,Γ,T₁)
    return rand(Binomial(M,(1-vis)/2+vis*Pe)), M
end

# to test functionality, we generate a prior particle representation of the TLS
# parameters and a simulated measurement function to go with it
function gen_test_TLS(N,ω=2π*5,g=2π*0.010,Γ=0,M=500,vis=0.95,T₁=14000)
    if Γ == 0
        D = MvNormal([ω+2g*(rand()-0.5),g*(1+0.5*(rand()-0.5))],[2g,0.5g])
    else
        D = MvNormal([ω+2g*(rand()-0.5),g*(1+0.5*(rand()-0.5)),Γ*(1+0.5*(rand()-0.5))],[2g,0.5g,0.25Γ])
    end
    X = abs.(rand(D,N)) # force positive values (may make the distribution non-normal)
    fun = (ν,t) -> swap_spectroscopy(ν,t,ω,g,Γ,M,vis,T₁)
    return X, fun
end

function calc_error(n=10,N=10000,ω=2π*5,g=2π*0.010,λ=50,μ=5,M=500)
    R = Matrix{Float64}(undef,n,3)
    percent_error_dist = r -> √((r[1]/ω-1)^2 + (r[2]/g-1)^2)
    for i = 1:n
        _X, fun1 = gen_test_TLS(N,ω,g,M)
        r = estimate_TLS_params(fun1,X[1:2,:],75,false)
        R[i,1] = percent_error_dist(r)
    end
    return R
end
