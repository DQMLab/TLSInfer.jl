# We'll just use some global variables in the module scope to avoid passing data
# back and forth between the experiment (MATLAB) and Julia

const INFERENCE_STEP = Ref{Int}(0)
const PRIOR_PARTICLE_DIST = Ref{Matrix{Float64}}()
const POSTERIOR_PARTICLE_DIST = Ref{Matrix{Float64}}()
const PARTICLE_WEIGHTS = Ref{Vector{Float64}}()
const CONTROLS = Ref{Vector{Float64}}()

function init_experiment(starting_distribution)
    INFERENCE_STEP[] = 1
    PRIOR_PARTICLE_DIST[] = Matrix{Float64}(starting_distribution)
    POSTERIOR_PARTICLE_DIST[] = copy(PRIOR_PARTICLE_DIST[])
    PARTICLE_WEIGHTS[] = Vector{Float64}(undef,size(PRIOR_PARTICLE_DIST[],2))
    CONTROLS[] = Float64[0,0]
    return nothing
end

function calc_measurement_controls(tmax=1E3)
    l = INFERENCE_STEP[]
    X = PRIOR_PARTICLE_DIST[]
    C = CONTROLS[]

    N = size(X,2) # number of particles

    # calculate mean and standard deviation of particles
    μ = vec(mean(X,dims=2))
    σ = sqrt.(abs.(diag(X*X'./N .- μ*μ')))

    # choose the measurement parameters ν,t (t is limited to tmax)
    ν,t = choose_measurement_controls(μ,σ,l,tmax)

    C[1:2] .= (ν,t)
    INFERENCE_STEP[] += 1
    return C
end

function update_distribution(e_count,M,timestep,tm,vis=0.9,T₁=14_000)
    X = PRIOR_PARTICLE_DIST[]
    Y = POSTERIOR_PARTICLE_DIST[]
    W = PARTICLE_WEIGHTS[]
    C = CONTROLS[]

    N = size(X,2) # number of particles
    μ = vec(mean(X,dims=2))
    ν,t = C

    # update particle distribution by calculating likelihoods
    W = particle_weights!(W,X,ν,t,e_count,M,vis,T₁,tm)

    # resample the particle distribution
    resample_particles!(Y,X,W)

    # what is new is old again!
    PRIOR_PARTICLE_DIST[],POSTERIOR_PARTICLE_DIST[] = Y,X

    # return current mean and stddev
    μ = vec(mean(Y,dims=2))
    σ = sqrt.(abs.(diag(Y*Y'./N .- μ*μ')))
    return [μ σ]
end
